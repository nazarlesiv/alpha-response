<?php
namespace Alpha\Response;

/*
 *Response Class
 */

class Response {
  private $_acceptHeaders = [];
  private $_statusSent = false;

  public function __construct($config = null) {

    /*
     * parse the request headers.
     * if the method is post/put/delete and the header type is json
     * parse the php raw input file and populate the request array.
     */
    $this->_acceptHeaders = $this->parseAcceptHeader();
  }

  public function __set($name, $value) {

  }

  public function __get($name) {

  }

  public function __toString() {

  }

  public function __destruct() {

  }

  //Check the accept headers sent by the client and respond with the proper format.
  public function send($data) {
    if (!empty($this->_acceptHeaders)) {
      if (is_array($this->_acceptHeaders)) {
        foreach($this->_acceptHeaders as $h) {
          switch ($h) {
            case 'application/json':
              return $this->json($data);
              break 2;
            case 'text/plain':
              return $this->html($data);
              break 2;
            default:
              return $this->json($data);
              break 2;
          }
        }
      }
    }
  }

  //if the exit flag it set to true, terminate the script after sending the code.
  public function statusCode($code, $_exit = false) {

    if (!$this->_statusSent) {

      //validate that the status code is not null and is a valid number.
      if ($code !== null && is_int($code)) {
        switch ($code) {
          case 100:
            $text = 'Continue';
            break;
          case 101:
            $text = 'Switching Protocols';
            break;
          case 200:
            $text = 'OK';
            break;
          case 201:
            $text = 'Created';
            break;
          case 202:
            $text = 'Accepted';
            break;
          case 203:
            $text = 'Non-Authoritative Information';
            break;
          case 204:
            $text = 'No Content';
            break;
          case 205:
            $text = 'Reset Content';
            break;
          case 206:
            $text = 'Partial Content';
            break;
          case 300:
            $text = 'Multiple Choices';
            break;
          case 301:
            $text = 'Moved Permanently';
            break;
          case 302:
            $text = 'Moved Temporarily';
            break;
          case 303:
            $text = 'See Other';
            break;
          case 304:
            $text = 'Not Modified';
            break;
          case 305:
            $text = 'Use Proxy';
            break;
          case 400:
            $text = 'Bad Request';
            break;
          case 401:
            $text = 'Unauthorized';
            break;
          case 402:
            $text = 'Payment Required';
            break;
          case 403:
            $text = 'Forbidden';
            break;
          case 404:
            $text = 'Not Found';
            break;
          case 405:
            $text = 'Method Not Allowed';
            break;
          case 406:
            $text = 'Not Acceptable';
            break;
          case 407:
            $text = 'Proxy Authentication Required';
            break;
          case 408:
            $text = 'Request Time-out';
            break;
          case 409:
            $text = 'Conflict';
            break;
          case 410:
            $text = 'Gone';
            break;
          case 411:
            $text = 'Length Required';
            break;
          case 412:
            $text = 'Precondition Failed';
            break;
          case 413:
            $text = 'Request Entity Too Large';
            break;
          case 414:
            $text = 'Request-URI Too Large';
            break;
          case 415:
            $text = 'Unsupported Media Type';
            break;
          case 500:
            $text = 'Internal Server Error';
            break;
          case 501:
            $text = 'Not Implemented';
            break;
          case 502:
            $text = 'Bad Gateway';
            break;
          case 503:
            $text = 'Service Unavailable';
            break;
          case 504:
            $text = 'Gateway Time-out';
            break;
          case 505:
            $text = 'HTTP Version not supported';
            break;
          default:
            exit('Unknown http status code \''.htmlentities($code).'\'');
            break;
        }

        //check if function 'http_response_code' exists, if so, send the code.
        if (function_exists('http_response_code')) {

          http_response_code($code);

        } else {
          //get the protocol from the server variables.
          $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
          //build and send the header.
          header($protocol.' '.$code.' '.$text);
        }

      } else {
        //if the code is invalide, exit with an unknown error
        echo('Unknown http status code \''.htmlentities($code).'\'');
      }
      //Set the flag that the statusCode has been sent.
      $this->_statusSent = true;
    }

    //exit the script unless the if the _exit flag is true.
    if ($_exit === true) {
      exit;
    }

    return $this;
  }

  public function status($code, $_exit = false) {
    return $this->statusCode($code, $_exit);
  }

  //send json encoded data.
  public function json($json) {

    //The only can only be a string or an array.
    if(!is_scalar($json) && !is_array($json)) {
      throw new Exception('Invalid type supplied. String or Array expected');
      return;
    }

    //if an array is supplied, encode_json, else assume
    // the parameter is already a json string.
    if (is_array($json)) {
      $json = json_encode($json);
    }

    $this->status(200);
    header('Content-type: application/json');
    echo($json);
    exit;
  }

  //send html
  public function html($data) {
    $this->status(200);
    //send the header.
    header('Content-type: text/html');
    echo($data);
    exit;
  }

  //build a query string from an assiciative array.
  public function buildQueryString($input = array()) {
    if (!is_array($input) || empty($input)) {
      return '';
    }

    $q = '';

    //build key, value pairs
    foreach($input as $key => $value) {
      $q .= $key.'='.$value.'&';
    }
    //return the string and trim the trailing &
    return trim($q, '&');
  }

  //redirect the request.
  public function redirect($path, $query = null) {
    $path = !is_string($path) || empty($path) ? '/' : $path;
    $query = !is_string($query) || empty($path) ? '' : $query;

    //build the location string
    $location = $path;
    $location .= isset($query) && !empty($query) ? '?'.$query : '';

    header('Location:'.$location);
    exit;
  }

  /*
   *@method render() - load the appropriate layout and render the data.
   *@param string $layout - the name of the layout to render
   *@param array $data - the array containing key value pairs with the data to be rendered.
   */
  public function render($layout = null, $data) {

    //load the layout file.
    include('./public /index.html');

    exit;

  }

  /*
   *@method parseAcceptHeader() - Parce the accept header and sort according to the q value.
   *@return array - assocciative array containing the header values and their q values.
   */
  private function parseAcceptHeader() {
    //If an HTTP_ACCETP header is not supplied, return empty array.
    if (!isset($_SERVER['HTTP_ACCEPT'])) {
      return [];
    }

    $header = $_SERVER['HTTP_ACCEPT'];
    $values = preg_split('/\s*,\s*/', $header);

    //Todo : Sort header values based on q values.

    $data_arr = array();
    foreach($values as $val) {
      $data_arr[$val] = $val;
    }

    return $data_arr;
  }
}
